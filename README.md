# Projeto de exemplo Angular usando Bootstrap

Projeto de exemplo das aulas de Angular na disciplina de desenvolvimento web avançado.

Site com todas as aulas:
https://gilbriatore.gitlab.io/2021/angular/site

Playlist do youtube de todas as aulas: 
https://www.youtube.com/watch?v=fEibky9RDUg&list=PLfqtDthdyWq7xbgrZXhUfxhgd2Ye94xLL

# Para rodar o projeto
0. Instalar no ambiente de desenvolvimento o Node.js e Angular.
1. Fazer o checkout do projeto utilizando: git clone https://gitlab.com/gilbriatore/2021/angular/ex-angular-bootstrap.git
2. Rodar, na pasta do projeto, o comando: npm install
3. Executar, na pasta do projeto, o servidor com o comando: ng serve --open
4. O servidor será iniciado em `http://localhost:4200/` e o navegador aberto automaticamente. 
